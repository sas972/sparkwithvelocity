package org.sas.sparkwithvelocity;

import static spark.Spark.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

public class HelloWorld {
	private static final Logger logger = LoggerFactory.getLogger(HelloWorld.class);
	private String buttonText = "Button";
	 	 
	private LocalDate date = LocalDate.now();
 
	 
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public static void main(String[] args) {
		List<Customer> customers = Arrays.asList(new Customer("Sas", 5000), new Customer("Pityu", 500),
												new Customer("Joska", 1000));
		
		
		get("/hello", (req, res) -> "Hello World");
		
		get("/helllo-velocity", (req, res) -> {
			logger.info("get: helllo-velocity");
		    Map<String, Object> model = new HashMap<>();
		    HelloWorld world = new HelloWorld();
		    return renderHello(customers, model, world);
		});
		
		post("/helllo-velocity", (req, res) -> {
			logger.info("post: helllo-velocity");
			String dateString = req.queryParams("startdate");
			logger.info(dateString);
			HelloWorld world = new HelloWorld();
			world.setDate(LocalDate.parse(dateString));
		    Map<String, Object> model = new HashMap<>();
		    return renderHello(customers, model, world);
		});
	}

	private static Object renderHello(List<Customer> customers, Map<String, Object> model, HelloWorld world) {
		model.put("message", "Velocity World");
		model.put("customerList", customers);
		model.put("cls", world);
		return new VelocityTemplateEngine().render(
		    new ModelAndView(model, "hello.vm")
		);
	}
	
	public void onClick(){
		logger.info("onClick!");
	}
	
}
